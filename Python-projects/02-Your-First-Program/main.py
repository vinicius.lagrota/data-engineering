# Task 1
print("Task 1:")
print("Hello, Python!")

# Task 2
print("\nTask 2:")
import sys
print(sys.version)

# Task 3
print("\nTask 3:")
print(type(1))
print(type(1.1))
print(type("string"))

# Task 4
print("\nTask 4:")
print(sys.float_info)

# Task 5
print("\nTask 5:")
print(type(float(2)))

# Task 6
print("\nTask 6:")
print(str(1))

# Task 7
print("\nTask 7:")
print(type(True))
print(bool(1))

# Task 8
print("\nTask 8:")
print(type(25/2))
print(type(25//2))

# Task 9
hour = 160/60
print("Hours: ", hour)