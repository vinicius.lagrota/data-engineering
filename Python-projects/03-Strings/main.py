# Task 1
print("Task 1:")
name = "Michael Jackson"
print(name)

# Task 2
print("\nTask 2:")
print(name[0])

# Task 3
print("\nTask 3:")
print(name[-1])

# Task 4
print("\nTask 4:")
print(name[0:5])

# Task 5
print("\nTask 5:")
print(name[::2])
print(name[3:10:3])

# Task 6
print("\nTask 6:")
statement = name + " is the best"
print(statement)

# Task 7
print("\nTask 7:")
name = name.upper()
print(name)
name = name.replace("MICHAEL", "JANET")
print(name)
print(name.find("ET"))
print(name.find("ET", 1))
print(name.find("ET", 5))
