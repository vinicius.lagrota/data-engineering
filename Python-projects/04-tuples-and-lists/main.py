# Task 1
a_list = [1, "hello", [1,2,3], True]
print(a_list)

# Taks 2
print(a_list[1])

# Task 3
print(a_list[1:4])

# Task 4
A = [1, 'a']
B = [2, 1, 'd']
print(A+B)

# Task 5
genres_tuple = ("pop", "rock", "soul", "hard rock", "soft rock", \
                "R&B", "progressive rock", "disco") 
print(len(genres_tuple))

# Task 6
print(genres_tuple[3])
            
# Task 7
print(genres_tuple[3:6])

# Task 8
print(genres_tuple[0:2])

# Task 9
print(genres_tuple.index("disco"))

# Task 10
C_tuple=(-5, 1, -3)
print(sorted(C_tuple))

