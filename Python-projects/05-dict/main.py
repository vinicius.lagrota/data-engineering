# Task 1
soundtrack_dic = {"The Bodyguard":"1992", "Saturday Night Fever":"1977"}
print(soundtrack_dic.keys())

# Task 2
print(soundtrack_dic.values())

# Task 3
album_sales_dict = {"Back in Black":50,"The Bodyguard":50,"Thriller":65}
print(album_sales_dict["Thriller"])

# Task 4
print(album_sales_dict.keys())

# Task 5
print(album_sales_dict.values())