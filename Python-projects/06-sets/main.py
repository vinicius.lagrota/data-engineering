# Task 1
set1 = set(['rap','house','electronic music', 'rap'])
print(set1)

# Task 2
A = [1, 2, 2, 1]
B = set([1, 2, 2, 1])
print(sum(A)==sum(B))

# Task 3
album_set1 = set(["Thriller", 'AC/DC', 'Back in Black'])
album_set2 = set([ "AC/DC", "Back in Black", "The Dark Side of the Moon"])
album_set3 = album_set1.union(album_set2)
print(album_set3)

# Task 4
print(album_set1.issubset(album_set3))

A=((1),[2,3],[4])
print(A[2])

print(len(("disco",10,1.2, "hard rock",10)) )