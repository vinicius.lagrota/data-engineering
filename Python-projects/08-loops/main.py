# Task 1
for i in range(-4,5):
    print(i)

# Task 2
Genres = ['rock', 'R&B', 'Soundtrack', 'R&B', 'soul', 'pop']
for Genre in Genres:
    print(Genre)

# Task 3
squares=['red', 'yellow', 'green', 'purple', 'blue']
for square in squares:
    print(square)

# Task 4
PlayListRatings = [10, 9.5, 10, 8, 7.5, 5, 10, 10]
i=0
while(i < len(PlayListRatings) and PlayListRatings[i] >= 6.0):
    print(PlayListRatings[i])
    i=i+1

# Taks 5
squares = ['orange', 'orange', 'purple', 'blue ', 'orange']
new_squares = []
i = 0
while(i < len(squares) and squares[i]=='orange'):
    new_squares.append(squares[i])
    i = i + 1
print(new_squares)