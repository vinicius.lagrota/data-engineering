# Task 1
def divide(x,y):
    return x/y

x = 10
y = 2
print(x, '/', y, '=', divide(x,y))

# Task 2
names = ("MJ", "AC/DC")

def ArtistNames(*name):
	for name in names:
		print(name)

ArtistNames(names)


# Task 3

def printDictionary(**args):
    for key in args:
        print(key + " : " + args[key])

printDictionary(Country='Canada',Province='Ontario',City='Toronto')
