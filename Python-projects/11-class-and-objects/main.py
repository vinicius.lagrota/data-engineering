import matplotlib.pyplot as plt

# Task 1
class circle(object):
    def __init__(self, radius, color):
        self.radius = radius
        self.color = color
    
    def add_radius(self, radius):
        self.radius = self.radius + radius

    def drawCircle(self):
        plt.gca().add_patch(plt.Circle((0, 0), radius=self.radius, fc=self.color))
        plt.axis('scaled')
        plt.show()  

RedCircle = circle(10, "red")
print(RedCircle.color)
RedCircle.add_radius(2)
print(RedCircle.radius)
RedCircle.drawCircle()