# Read
from asyncore import read, write


with open("example1.txt", "r") as file1:
    print(file1.name)
    print(file1.mode)
    file_content = file1.read()
    print(file_content)

with open("example1.txt", "r") as file1:
    for line in file1:
        print("Line 1: ", line)

with open("example1.txt", "r") as file1:
    fileList = file1.readlines()
print("Second item of the list:", fileList[1])

# Write
with open("example1.txt", "r") as readfile:
    with open("example2.txt", "w") as writefile:
        for line in readfile:
            writefile.write(line)

with open("example2.txt", "r") as wrotefile:
    fileContent = wrotefile.read()
    print(fileContent)