from initProblem import genFiles

'''
The two arguments for this function are the files:
    - currentMem: File containing list of current members
    - exMem: File containing list of old members
    
    This function should remove all rows from currentMem containing 'no' 
    in the 'Active' column and appends them to exMem.
    '''
def cleanFiles(currentMem, exMem):
    # Open the currentMem file as in r+ mode
    memberList = []
    with open(currentMem, "r+") as membersFile:   
        next(membersFile)
        for line in membersFile:  
            memberList.append(line)

    # Open the exMem file in a+ mode
    with open(exMem, "a+") as inactiveFile:
        for line in memberList:  
            if(line.find("no")) >= 0:
                inactiveFile.write(line)

    # Remove members from currentMem
    headers = "Membership No  Date Joined  Active  \n"
    with open(currentMem, "w+") as membersFile:
        membersFile.write(headers)
        for line in memberList:  
            if(line.find("yes")) >= 0:
                membersFile.write(line)
    

# The code below is to help you view the files.
# Do not modify this code for this exercise.
memReg = 'members.txt'
exReg = 'inactive.txt'

genFiles(memReg, exReg)

cleanFiles(memReg,exReg)


headers = "Membership No  Date Joined  Active  \n"
# with open(memReg,'r') as readFile:
    # print("Active Members: \n\n")
    # print(readFile.read())
    
# with open(exReg,'r') as readFile:
#     print("Inactive Members: \n\n")
#     print(readFile.read())
                