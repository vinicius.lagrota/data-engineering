import pandas as pd

# Task 1
data = {"Student":["David", "Samuel", "Terry", "Evan"], 
        "Age":[27, 24, 22, 32], 
        "Country":["UK", "Canada", "China", "USA"], 
        "Course":["Python", "Data Structures", "Machine Learning", "Web Development"], 
        "Marks":[85, 72, 89, 76]}

df = pd.DataFrame(data)
print(df)

b = df[['Marks']]
print(b)

c = df[['Country', 'Course']]
print(c)

x = df['Student']
print(x)

print(df.loc[0, 'Student'])
print(df.iloc[0, 0])

df = df.set_index("Student")
print(df.loc['David', 'Marks'])

df = df.reset_index()
print(df.iloc[2:3, 3:5])
print(df.loc[1:2, 'Country':'Marks'])

df = df.set_index("Student")
print(df.loc['Terry':'Evan', 'Country':'Marks'])

print(df.iloc[2:3, 1])