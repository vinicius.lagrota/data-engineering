import numpy as np

u = np.array([1,0])
v = np.array([0,1])
print(u - v)

z = np.array([2, 4])
z = z * -2
print(z)

u = np.array([1, 2, 3, 4, 5])
v = np.array([1, 0, 1, 0, 1])
print(u * v)

import time 
import sys
import matplotlib.pyplot as plt

def Plotvec2(a,b):
    ax = plt.axes()# to generate the full window axes
    ax.arrow(0, 0, *a, head_width=0.05, color ='r', head_length=0.1)#Add an arrow to the  a Axes with arrow head width 0.05, color red and arrow head length 0.1
    plt.text(*(a + 0.1), 'a')
    ax.arrow(0, 0, *b, head_width=0.05, color ='b', head_length=0.1)#Add an arrow to the  b Axes with arrow head width 0.05, color blue and arrow head length 0.1
    plt.text(*(b + 0.1), 'b')
    plt.ylim(-2, 2)#set the ylim to bottom(-2), top(2)
    plt.xlim(-2, 2)#set the xlim to left(-2), right(2)
    plt.show()

a = np.array([-1,1])
b = np.array([1,1])
Plotvec2(a,b)
print("The dot product is", np.dot(a,b))

a = np.array([1,0])
b = np.array([0,1])
Plotvec2(a,b)
print("The dot product is", np.dot(a,b))
