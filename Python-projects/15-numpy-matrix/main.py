import numpy as np

a = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
A = np.array(a)
print(A)
print(np.size(A))
print(A[0,0:2])

B = np.array([[0, 1], [1, 0], [1, 1], [-1, 0]])
print(B)
print(np.size(B))
print(B[0,0:2])

print("Shape A: ", np.shape(A))
print("Shape B: ", np.shape(B))

C = np.dot(A,B)
print(C)
