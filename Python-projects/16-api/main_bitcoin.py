
# bMOJOOFLSqeTXJU54-e8rI8SmAW79Wz-xigmGYQf1DbC
# https://api.us-south.language-translator.watson.cloud.ibm.com/instances/ba85fc4c-8e96-427b-97fe-222751d14622

# {
#   "apikey": "7nDotn6IWsMfS3Ln6QiBoat69d-3y5NHM5aCptAuFNiB",
#   "iam_apikey_description": "Auto-generated for key crn:v1:bluemix:public:speech-to-text:us-south:a/292d94f009dc417bb078a12482c56b75:abe1b20c-8b2e-4d04-b90f-42034bdc3745:resource-key:9858d648-1a03-4ffb-9744-fa1d6d99f1ac",
#   "iam_apikey_name": "Auto-generated service credentials",
#   "iam_role_crn": "crn:v1:bluemix:public:iam::::serviceRole:Manager",
#   "iam_serviceid_crn": "crn:v1:bluemix:public:iam-identity::a/292d94f009dc417bb078a12482c56b75::serviceid:ServiceId-0988c6a5-b40b-4cc1-9a3f-33a30a50ffb6",
#   "url": "https://api.us-south.speech-to-text.watson.cloud.ibm.com/instances/abe1b20c-8b2e-4d04-b90f-42034bdc3745"
# }


import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.offline import plot
import matplotlib.pyplot as plt
import datetime
from pycoingecko import CoinGeckoAPI
from mplfinance.original_flavor import candlestick2_ohlc

cg = CoinGeckoAPI()

bitcoin_data = cg.get_coin_market_chart_by_id(id='bitcoin', vs_currency='usd', days=30)

bitcoin_price_data = bitcoin_data['prices']
bitcoin_price_data[0:5]

data = pd.DataFrame(bitcoin_price_data, columns=['TimeStamp', 'Price'])
print(data)

data['date'] = data['TimeStamp'].apply(lambda d: datetime.date.fromtimestamp(d/1000.0))
candlestick_data = data.groupby(data.date, as_index=False).agg({"Price": ['min', 'max', 'first', 'last']})
fig = go.Figure(data=[go.Candlestick(x=candlestick_data['date'],
                open=candlestick_data['Price']['first'], 
                high=candlestick_data['Price']['max'],
                low=candlestick_data['Price']['min'], 
                close=candlestick_data['Price']['last'])
                ])

fig.update_layout(xaxis_rangeslider_visible=False)

fig.show()







