import requests
import os 
#from PIL import Image
from IPython.display import IFrame

url='https://www.ibm.com/'
r=requests.get(url)
print(r.status_code)
header=r.headers
print(r.headers)

# Get
url_get='http://httpbin.org/get'
payload={"name":"Joseph","ID":"123"}
r=requests.get(url_get,params=payload)
print(r.json)

# Post
url_post='http://httpbin.org/post'
r_post=requests.post(url_post,data=payload)
print("POST request URL:",r_post.url )
print("GET request URL:",r.url)
print("POST request body:",r_post.request.body)
print("GET request body:",r.request.body)

print('End')