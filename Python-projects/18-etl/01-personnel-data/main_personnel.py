import glob
import pandas as pd
import xml.etree.ElementTree as ET
from datetime import datetime

print("Starting...")

# Paths
tmpfile = "01-personnel-data/temp.tmp"
logfile = "01-personnel-data/logfile.txt"
targetfile = "01-personnel-data/transformed_data.csv"

#
#   Functions
#

# Extract data from CSV
def extract_from_csv(file_to_process):
    dataframe = pd.read_csv(file_to_process)
    return dataframe

# Extract data from JSON
def extract_from_json(file_to_process):
    dataframe = pd.read_json(file_to_process, lines=True)
    return dataframe    

# Extract data from XML
def extract_from_xml(file_to_process):
    dataframe = pd.DataFrame(columns=["name", "height", "weight"])
    tree = ET.parse(file_to_process)
    root = tree.getroot()
    for person in root:
            name = person.find("name").text
            height = float(person.find("height").text)
            weight = float(person.find("weight").text)
            dataframe = dataframe.append({"name":name, "height":height, "weight":weight}, ignore_index=True)
    return dataframe

# Extract function
def extract():

    extracted_data = pd.DataFrame(columns=['name', 'height', 'weight'])

    for csvfile in glob.glob("01-personnel-data/person_data/*.csv"):
        extracted_data = extracted_data.append(extract_from_csv(csvfile), ignore_index=True)

    for jsonfile in glob.glob("01-personnel-data/person_data/*.json"):
        extracted_data = extracted_data.append(extract_from_json(jsonfile), ignore_index=True)

    for xmlfile in glob.glob("01-personnel-data/person_data/*.xml"):
        extracted_data = extracted_data.append(extract_from_xml(xmlfile), ignore_index=True)

    return extracted_data

# Transform
def transform(data):

    i = 0
    for number in data.height:
        number = round(number * 0.0254, 2)
        data['height'][i] = number
        i = i + 1

    i = 0
    for number in data.weight:
        number = round(number * 0.45359237, 2)
        data['weight'][i] = number
        i = i + 1
        
    return data

# Load
def load(targetfile, data_to_load):
    data_to_load.to_csv(targetfile)

# Log
def log(message):
    timestamp_format = '%Y-%h-%d-%H:%M:%S' # Year-Monthname-Day-Hour-Minute-Second
    now = datetime.now() # get current timestamp
    timestamp = now.strftime(timestamp_format)
    with open(logfile,"a") as f:
        f.write(timestamp + ',' + message + '\n')

#
#   ETL
#
log("ETL Job Started")

log("Extract phase Started")
extracted_data = extract()
print(extracted_data)
log("Extract phase Ended")

log("Transform phase Started")
transformed_data = transform(extracted_data)
print(transformed_data)
log("Transform phase Ended")

log("Load phase Started")
load(targetfile, transformed_data)
log("Load phase Ended")

log("ETL Job Ended")

print("Finished.")