import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
from datetime import datetime

print("Starting...")

# Paths
tmpfile    = "02-car-dealership/dealership_temp.tmp"               # file used to store all extracted data
logfile    = "02-car-dealership/dealership_logfile.txt"            # all event logs will be stored in this file
targetfile = "02-car-dealership/dealership_transformed_data.csv"   # file where transformed data is stored

#
#   Functions
#

# Extract data from CSV
def extract_from_csv(file_to_process):
    dataframe = pd.read_csv(file_to_process)
    print(dataframe)
    return dataframe

# Extract data from JSON
def extract_from_json(file_to_process):
    dataframe = pd.read_json(file_to_process, lines=True)
    return dataframe    

# Extract data from XML
def extract_from_xml(file_to_process):
    dataframe = pd.DataFrame(columns=["car_model", "year_of_manufacture", "price", "fuel"])
    tree = ET.parse(file_to_process)
    root = tree.getroot()
    for car in root:
            car_model = car.find("car_model").text
            year_of_manufacture = car.find("year_of_manufacture").text
            price = float(car.find("price").text)
            fuel = car.find("fuel").text
            dataframe = dataframe.append({"car_model":car_model, "year_of_manufacture":year_of_manufacture, "price":price, "fuel":fuel}, ignore_index=True)
    return dataframe

# Extract function
def extract():

    extracted_data = pd.DataFrame(columns=['car_model', 'year_of_manufacture', 'price', 'fuel'])

    for csvfile in glob.glob("02-car-dealership/dealership_data/*.csv"):
        extracted_data = extracted_data.append(extract_from_csv(csvfile), ignore_index=True)

    for jsonfile in glob.glob("02-car-dealership/dealership_data/*.json"):
        extracted_data = extracted_data.append(extract_from_json(jsonfile), ignore_index=True)

    for xmlfile in glob.glob("02-car-dealership/dealership_data/*.xml"):
        extracted_data = extracted_data.append(extract_from_xml(xmlfile), ignore_index=True)

    return extracted_data

# Transform
def transform(data):

    i = 0
    for number in data.price:
        data['price'][i] = round(number, 2)
        i = i + 1
        
    return data

# Load
def load(targetfile, data_to_load):
    if os.path.exists(targetfile):
        os.remove(targetfile)
        print("Erasing previous data...")
    
    data_to_load.to_csv(targetfile)

# Log
def log(message):
    timestamp_format = '%Y-%h-%d-%H:%M:%S' # Year-Monthname-Day-Hour-Minute-Second
    now = datetime.now() # get current timestamp
    timestamp = now.strftime(timestamp_format)
    with open(logfile,"a") as f:
        f.write(timestamp + ',' + message + '\n')


#
#   ETL
#
log("ETL Job Started")

log("Extract phase Started")
extracted_data = extract()
print(extracted_data)
log("Extract phase Ended")

log("Transform phase Started")
transformed_data = transform(extracted_data)
print(transformed_data)
log("Transform phase Ended")

log("Load phase Started")
load(targetfile, transformed_data)
log("Load phase Ended")

log("ETL Job Ended")

print("Finished.")