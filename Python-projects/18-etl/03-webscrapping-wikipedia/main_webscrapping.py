from bs4 import BeautifulSoup
import html5lib
import requests
import pandas as pd

## Extract ##

# Get page content
url = 'https://en.wikipedia.org/wiki/List_of_largest_banks'
html_data = requests.get(url).text
print(html_data[101:124])

# Use soup to find table
soup = BeautifulSoup(html_data, 'html.parser')

# Data frame
data = pd.DataFrame(columns=["Name", "Market Cap (US$ Billion)"])

for row in soup.find_all('tbody')[3].find_all('tr'): # Third table
    col = row.find_all('td')
    if(len(col) > 0):
        name = col[1].contents[2].text
        market_cap = col[2].contents[0].rstrip("\n")
        data = data.append({"Name":name, "Market Cap (US$ Billion)":market_cap}, ignore_index=True)

print(data.head(5))

## Load ##
data.to_json("03-webscrapping-wikipedia/bank_market_cap.json")

print("End")