import requests
import pandas as pd

# API
url = "https://api.apilayer.com/exchangerates_data/latest?base=EUR&apikey=A0sBMffyRZeYTY960OecTk3xJJYvcxHq"
req = requests.get(url).text

## Extract ##
dataframe = pd.read_json(req)
# Drop unnecessary columns
dataframe = dataframe.drop('success', 1).drop('timestamp', 1).drop('base', 1).drop('date', 1)
print(dataframe)

## Load ##
dataframe.to_csv("04-extract-using-api/exchange_rates_1.csv")

print("Fim")
