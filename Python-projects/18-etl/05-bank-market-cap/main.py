import glob
import pandas as pd
from datetime import datetime

## Functions ##
def extract_from_json(file_to_process):
    dataframe = pd.read_json(file_to_process)
    return dataframe

# Extract
def extract():
    columns=['Name','Market Cap (US$ Billion)']
    dataframe = pd.DataFrame(columns=columns)
    json_path = "05-bank-market-cap/bank_market_cap_1.json"
    dataframe = dataframe.append(extract_from_json(json_path), ignore_index=True);
    return dataframe

# Transform
def transform(df, exchange_rate):
    for index, row in df.iterrows():
        df.at[index,'Market Cap (US$ Billion)'] = round(df.at[index,'Market Cap (US$ Billion)']*exchange_rate, 3)

    df.rename(columns = {'Market Cap (US$ Billion)':'Market Cap (GBP$ Billion)'}, inplace = True)
    return df

# Load
def load(df, targetfile):
    df.to_csv(targetfile)

# Log
def log(message):
    timestamp_format = '%Y-%h-%d-%H:%M:%S' # Year-Monthname-Day-Hour-Minute-Second
    now = datetime.now() # get current timestamp
    timestamp = now.strftime(timestamp_format)
    with open("05-bank-market-cap/log.txt","a") as f:
        f.write(timestamp + ',' + message + '\n')

## Main ##

log("ETL Job Started")

log("Extract phase Started")
df = extract()
print(df.head(5))
log("Extract phase Ended")

# Extract exchange rates
df_exchange = pd.read_csv("05-bank-market-cap/exchange_rates.csv", index_col=0)
exchange_rate = df_exchange.loc['GBP']['Rates']
print(exchange_rate)

# Transform
log("Transform phase Started")
df = transform(df, exchange_rate)
print(df.head(5))
log("Transform phase Ended")
# print(df)

# Load
log("Load phase Started")
load(df, "05-bank-market-cap/bank_market_cap_gbp.csv")
log("Load phase Ended")

print("Fim")