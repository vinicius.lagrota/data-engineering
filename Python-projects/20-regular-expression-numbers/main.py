import re

# Read txt file
with open('actual_data.txt', 'r') as f:
    text = f.read()

# Find number in text
list = re.findall('[0-9]+', text)

# Convert string to integer
listNum = [int(i) for i in list]

# Sum all numbers in list and print
print(sum(listNum))

