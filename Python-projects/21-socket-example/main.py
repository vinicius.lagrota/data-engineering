import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# mysock.connect(('support.mozilla.org', 433))
# cmd = 'GET https://support.mozilla.org/en-US/kb/captive-portal HTTP/2\r\n\r\n'.encode()

# mysock.connect(('detectportal.firefox.com', 80))
# cmd = 'GET http://detectportal.firefox.com/canonical.html HTTP/2\r\n\r\n'.encode()

mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()

mysock.send(cmd)


while True:
    data = mysock.recv(512)
    if(len(data) < 1):
        break;
    print(data.decode())
mysock.close()