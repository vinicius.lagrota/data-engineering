import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl
import re

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter - ')
# url = 'http://py4e-data.dr-chuck.net/known_by_Fikret.html'
count = int(input('Count - '))
# count = 4
position = int(input('Position - '))
# position = 3
name = list()

for i in range(count):
    
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    tags = soup('a')
    print(tags[position-1])
    print('Retrieving:', tags[position-1].get('href', None))
    print('Contents:', tags[position-1].contents[0])    
    url = tags[position-1].get('href', None)
    name.append(tags[position-1].contents[0])
    print(name)