import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = 'http://py4e-data.dr-chuck.net/comments_1586892.xml'
print('Retrieving', url)
xml_data = urllib.request.urlopen(url, context=ctx).read()
data = xml_data.decode()
stuff = ET.fromstring(data)
lst = stuff.findall('.//count')

sum = 0
for elements in lst:
    sum = sum + int(elements.text)

print(sum)