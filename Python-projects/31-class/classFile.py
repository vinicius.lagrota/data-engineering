class Person():
    x = 0;
    name = ""

    def __init__(self, nam): # __init__ is optional
        self.name = nam
        print("Class person:", self.name)

    def counter(self):
        self.x = self.x + 1
        print("Person counter: ", self.x)

    def setCounter(self, value):
        self.x = value
        print("Person counter: ", self.x)

class Sports(Person): # Sport class extends Person class: the former contais all of the latter plus the following methods.
    points = 0

    def scoreTouchdown(self):
        self.points = self.points + 7
        print("Touchdown points:", self.points)
