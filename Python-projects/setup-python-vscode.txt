https://code.visualstudio.com/docs/python/python-tutorial

-- Tutorial rápido:
1) Criar uma pasta no local desejado e abrir essa pasta no VSCode.
2) Criar o arquivo .py.
3) Criar um ambiente virtual para os pacotes não se embaralharem:
	python3 -m venv <nome do venv>
	source <nome do venv>/bin/activate
4) Selecionar o novo venv clicando em Yes no pop-up ou:
	ctrl+shif+p e digitar Python: Select Interpreter
5) Desative o ambiente virtual com o comando deactivate	
	
-- Instalar dependencias:
1) Para instalar somente um pacote, use:
	apt-get install python3-tk //Só precisa rodar 1x para instalar.
	python3 -m pip install <pacote>
	
-- Para pegar todas os requerimentos e passar para outro computador, usar:
1) Criar um arquivo de requisições:
	pip freeze > requirements.txt
3) Rodar o comando nno outro computador:
	pip install -r requirements.txt


